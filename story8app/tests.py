from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import book


# Create your tests here.
class LibraryTest(TestCase):

    def test_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'library.html')

    def test_header(self):
        request = HttpRequest()
        response = book(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Library", html_response)


class FuncionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000/')

    def tearDown(self):
        self.selenium.quit()
