from django.urls import path
from .views import book, data

appname = 'Story8App'

urlpatterns = [
    path('', book, name='bookList'),
    path('data/', data, name='books_data'),
]